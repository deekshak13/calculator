package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.security.AccessController;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText num1,num2;
    private Button add, sub,mul,div;
    private TextView result;
    float val1,val2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1=findViewById(R.id.editText1);
        num2=findViewById(R.id.editText2);
        sub=findViewById(R.id.btnSub);
        add=findViewById(R.id.btnAdd);
        mul=findViewById(R.id.btnMul);
        div=findViewById(R.id.btnDiv);
        result=findViewById(R.id.textView);


        add.setOnClickListener(this);
        sub.setOnClickListener(this);
        mul.setOnClickListener(this);
        div.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        try {
            val1 = Float.parseFloat(num1.getText().toString());
            val2 = Float.parseFloat(num2.getText().toString());
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Please enter both values",Toast.LENGTH_LONG).show();
        }
       
        switch (v.getId())
        {
            case R.id.btnAdd:
                val1=val1+val2;
                result.setText(String.valueOf(val1));
                break;

            case R.id.btnSub:
                float sub=val1-val2;
                result.setText(String.valueOf(sub));
                break;

            case R.id.btnMul:
                float mul=val1*val2;
                result.setText(String.valueOf(mul));
                break;

            case R.id.btnDiv:
                if (val2 == 0) {
                    result.setText("Cannot divide by 0");
                } else {
                    float div = val1 / val2;
                    result.setText(String.valueOf(div));
                }


        }
    }
}